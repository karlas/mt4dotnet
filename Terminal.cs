﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Threading;
using System.Diagnostics;

namespace MT4DotNet
{
    public class Terminal : IDisposable
    {
        internal readonly object ConnectionLock = new object();
        private NamedPipeServerStream connection;
        public Account Account { get; private set; }
        public Dictionary<string, Symbol> Symbols { get; private set; }
        Timer timer;
        string connectionName = "";
        public Terminal(string connectionName, int timeout = 1000)
        {
            this.connectionName = connectionName;
            Connect(timeout);
            Account = new Account(this);

            Symbols = new Dictionary<string, Symbol>();
            foreach (var symbol in Symbol.GetSymbolList(this))
                Symbols.Add(symbol.Name, symbol);
            Orders = Order.GetAllOrders(this);
            foreach (var order in Orders)
            {
                if (order.MagicNumber == 4235236)
                {
                    FakeOrder = order;
                    fakeOrderValue = FakeOrder.OpenPrice;
                }
            }
            timer = new Timer(new TimerCallback((o) =>
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.IsStopped);
                }
            }), null, 2000, 2000);
        }

        public List<Order> Orders = null;

        private void Connect(int timeout)
        {
            if (connection != null)
            {
                connection.Close();
                connection = null;
            }
            connection = new NamedPipeServerStream(connectionName, PipeDirection.InOut, 1, PipeTransmissionMode.Byte);
            //TODO: Check if MT4 FileWriteString/FileReadString support UTF8
            Reader = new BinaryReader(connection, Encoding.ASCII);
            Writer = new BinaryWriter(connection, Encoding.ASCII);
            Thread thread = new Thread(new ThreadStart(() =>
            {
                try
                {
                    connection.WaitForConnection();
                }
                catch
                {
                }
            }));
            thread.Start();

            if (!thread.Join(timeout))
            {
                connection.Close();
                if (ConnectionStatus != null)
                    ConnectionStatus(this);
                throw new Exception("Failed to connect with MetaTrader.");
            }
            else
            {
                if (ConnectionStatus != null)
                    ConnectionStatus(this);
            }
        }

        #region Terminal calls

        /// <summary>
        /// The function returns the status of the main connection between ddeClientAsk terminal and server that performs data pumping.
        /// It returns TRUE if connection to the server was successfully established, otherwise, it returns FALSE.
        /// </summary>
        public bool IsConnectedToMT4Server
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.IsConnected);
                    return ReadByte() > 0;
                }
            }
        }

        /// <summary>
        /// Returns TRUE if the expert runs on a demo account, otherwise returns FALSE.
        /// </summary>
        public bool IsDemo
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.IsDemo);
                    return ReadByte() > 0;
                }
            }
        }

        /// <summary>
        /// Returns TRUE if the function DLL call is allowed for the expert, otherwise returns FALSE.
        /// </summary>
        public bool IsDllsAllowed
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.IsDllsAllowed);
                    return ReadByte() > 0;
                }
            }
        }

        /// <summary>
        /// Returns TRUE if expert adwisors are enabled for running, otherwise returns FALSE.
        /// </summary>
        public bool IsExpertEnabled
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.IsExpertEnabled);
                    return ReadByte() > 0;
                }
            }
        }

        /// <summary>
        /// Returns TRUE if the expert can call library function, otherwise returns FALSE.
        /// </summary>
        public bool IsLibrariesAllowed
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.IsLibrariesAllowed);
                    return ReadByte() > 0;
                }
            }
        }

        /// <summary>
        /// Returns TRUE if expert runs in the strategy tester optimization mode, otherwise returns FALSE.
        /// </summary>
        public bool IsOptimization
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.IsOptimization);
                    return ReadByte() > 0;
                }
            }
        }

        //We are polling every 2 seconds this command
        ///// <summary>
        ///// Returns TRUE if the program (an expert or a script) has been commanded to stop its operation, otherwise returns FALSE. The program can continue operation for 2.5 seconds more before the ddeClientAsk terminal stops its performing forcedly.
        ///// </summary>
        //public bool IsStopped
        //{
        //    get
        //    {
        //        lock (ConnectionLock)
        //        {
        //            Write(N2MTCommand.IsStopped);
        //            return ReadByte() > 0;
        //        }
        //    }
        //}

        /// <summary>
        /// Returns TRUE if expert runs in the testing mode, otherwise returns FALSE.
        /// </summary>
        public bool IsTesting
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.IsTesting);
                    return ReadByte() > 0;
                }
            }
        }

        /// <summary>
        /// Returns TRUE if the expert is allowed to trade and a thread for trading is not occupied, otherwise returns FALSE.
        /// </summary>
        public bool IsTradeAllowed
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.IsTradeAllowed);
                    return ReadByte() > 0;
                }
            }
        }

        //TODO: Checkout what this is and how it should be used...
        /// <summary>
        /// Returns TRUE if a thread for trading is occupied by another expert advisor, otherwise returns FALSE.
        /// </summary>
        public bool IsTradeContextBusy
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.IsTradeContextBusy);
                    return ReadByte() > 0;
                }
            }
        }

        /// <summary>
        /// Returns TRUE if the expert is tested with checked "Visual Mode" button, otherwise returns FALSE.
        /// </summary>
        public bool IsVisualMode
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.IsVisualMode);
                    return ReadByte() > 0;
                }
            }
        }

        public enum UninitializeReasons
        {
            /// <summary>
            /// Script finished its execution independently.
            /// </summary>
            Normal,
            /// <summary>
            /// Expert removed from chart.
            /// </summary>
            Remove,
            /// <summary>
            /// Expert recompiled.
            /// </summary>
            Recompile,
            /// <summary>
            /// Symbol or timeframe changed on the chart.
            /// </summary>
            ChartChange,
            /// <summary>
            /// Chart closed.
            /// </summary>
            ChartClose,
            /// <summary>
            /// Inputs parameters was changed by user.
            /// </summary>
            ParametersChanged,
            /// <summary>
            /// Other account activated.
            /// </summary>
            AccountChanged
        }

        /// <summary>
        /// Returns the code of the uninitialization reason for the experts, custom indicators, and scripts. 
        /// The returned values can be ones of Uninitialize reason codes. 
        /// This function can also be called in function init() to analyze the reasons for deinitialization of the previour launch.
        /// </summary>
        public UninitializeReasons UninitializeReason
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.IsConnected);
                    return (UninitializeReasons)ReadByte();
                }
            }
        }

        /// <summary>
        /// Returns the name of company owning the ddeClientAsk terminal.
        /// </summary>
        public string Company
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.TerminalCompany);
                    return ReadString();
                }
            }
        }

        /// <summary>
        /// Returns ddeClientAsk terminal name.
        /// </summary>
        public string Name
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.TerminalName);
                    return ReadString();
                }
            }
        }

        /// <summary>
        /// Returns the directory, from which the ddeClientAsk terminal was launched.
        /// </summary>
        public string StartupPath
        {
            get
            {
                lock (ConnectionLock)
                {
                    Write(N2MTCommand.TerminalPath);
                    return ReadString();
                }
            }
        }

        #endregion


        #region Writer functions

        private BinaryWriter Writer;
        private bool disposed = false;
        internal void Write(N2MTCommand command)
        {
            if (disposed)
                throw new ObjectDisposedException("terminal");
            try
            {
                Writer.Write((byte)command);
            }
            catch
            {
                Connect(400);
                Writer.Write((byte)command);
            }
        }

        internal void Write(byte data)
        {
            Writer.Write(data);
        }

        internal void Write(int data)
        {
            Writer.Write(data);
        }

        internal void Write(double data)
        {
            Writer.Write(data);
        }

        internal void Write(string data)
        {
            //TODO: Implement Read7BitEncodedInt and Write7BitEncodedInt to MQL4 to support more then 127 chars long strings
            if (data.Length > 127)
                throw new Exception("At this moment only strings shorter then 128 chars are allowed. This can be fixed if wanted but requiers some work.");
            Writer.Write(data);
        }

        #endregion

        #region Reader functions
        private BinaryReader Reader;
        private Order FakeOrder;

        internal MT2NCommand ReadCommand()
        {
            return (MT2NCommand)Reader.ReadByte();
        }

        internal double ReadDouble()
        {
            return Reader.ReadDouble();
        }

        internal int ReadInt32()
        {
            return Reader.ReadInt32();
        }


        #endregion

        internal string ReadString()
        {
            return Reader.ReadString();
        }

        internal byte ReadByte()
        {
            return Reader.ReadByte();
        }

        internal void Write(Color? color)
        {
            if (color.HasValue)
                Write((color.Value.R << 16) | (color.Value.G << 8) | color.Value.B);
            else
                Write(-1);
        }

        internal void Write(byte[] p, int index = -1, int count = -1)
        {
            if (index == -1)
                connection.Write(p, 0, p.Length);

        }

        public bool Connected
        {
            get
            {
                return connection != null && connection.IsConnected;
            }
        }

        double fakeOrderValue = 0;
        public void SendFakeOrder()
        {
            if (FakeOrder == null)
            {
                fakeOrderValue = 1;
                FakeOrder = new Order(this, Symbols["USDJPY"], OrderType.BuyLimit, 0.1, 1, 5, 0.1, 1.9, "FakeOrder", 4235236);
            }
            else
            {
                if (fakeOrderValue == 1)
                {
                    fakeOrderValue = 1.1;
                    FakeOrder.Modify(1.1, 0.1, 1.9);
                }
                else
                {
                    fakeOrderValue = 1;
                    FakeOrder.Modify(1, 0.1, 1.9);
                }
            }
        }

        internal void Write(TimeSpan? timeSpan)
        {
            if (timeSpan.HasValue)
                Writer.Write((int)timeSpan.Value.TotalSeconds);
            else
                Writer.Write(0);
        }

        public void Dispose()
        {
            if (timer != null)
            {
                timer.Dispose();
                timer = null;
            }
            if (connection != null)
            {
                connection.Dispose();
                connection = null;
                if (ConnectionStatus != null)
                    ConnectionStatus(this);
            }
            disposed = true;
        }

        public static string EX4FileName
        {
            get
            {
                return "MT4DotNet.ex4";
            }
        }

        public event Action<Terminal> ConnectionStatus;
    }
}
