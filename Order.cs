﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MT4DotNet
{
    public enum OrderType : byte
    {
        /// <summary>
        /// Buying position.
        /// </summary>
        Buy = 0,
        /// <summary>
        /// Selling position.
        /// </summary>
        Sell = 1,
        /// <summary>
        /// Buy limit pending position.
        /// </summary>
        BuyLimit = 2,
        /// <summary>
        /// Sell limit pending position.
        /// </summary>
        SellLimit = 3,
        /// <summary>
        /// Buy stop pending position.
        /// </summary>
        BuyStop = 4,
        /// <summary>
        /// Sell stop pending position.
        /// </summary>
        SellStop = 5
    }

    public class Order
    {
        Terminal terminal;
        int ticket;
        private int magicNumber;
        internal Order(Terminal terminal, int ticket)
        {
            this.terminal = terminal;
            this.ticket = ticket;
        }

        /// <summary>
        /// The main function used to open a position or place a pending order.
        /// </summary>
        /// <param name="terminal">Terminal on which order is being created.</param>
        /// <param name="symbol">Symbol for trading</param>
        /// <param name="orderType">Operation type. It can be any of the OrderType.</param>
        /// <param name="volume">Number of lots</param>
        /// <param name="price">Preferred price of the trade</param>
        /// <param name="slippage">Maximum price slippage for buy or sell orders</param>
        /// <param name="stopLoss">Stop loss level</param>
        /// <param name="takeProfit">Take profit level</param>
        /// <returns></returns>
        public Order(Terminal terminal, Symbol symbol, OrderType orderType, double volume, double price, byte slippage, double stopLoss, double takeProfit)
        {
            lock (terminal.ConnectionLock)
            {
                this.terminal = terminal;
                terminal.Write(N2MTCommand.OrderSend);
                terminal.Write(symbol.Name);
                terminal.Write((byte)orderType);
                terminal.Write(volume);
                terminal.Write(price);
                terminal.Write(slippage);
                terminal.Write(stopLoss);
                terminal.Write(takeProfit);
                var command = terminal.ReadCommand();
                if (command == MT2NCommand.Error)
                    throw new MT4Exception(terminal.ReadInt32());
                else
                    ticket = terminal.ReadInt32();
                terminal.Orders.Add(this);
            }
        }

        public Order(Terminal terminal, Symbol symbol, OrderType orderType, double volume, double price, byte slippage, double stopLoss, double takeProfit, string comment, int magic = 0, TimeSpan? expiration = null, Color? arrow_color = null)
        {
            lock (terminal.ConnectionLock)
            {
                this.terminal = terminal;
                terminal.Write(N2MTCommand.OrderSend2);
                terminal.Write(symbol.Name);
                terminal.Write((byte)orderType);
                terminal.Write(volume);
                terminal.Write(price);
                terminal.Write(slippage);
                terminal.Write(stopLoss);
                terminal.Write(takeProfit);
                terminal.Write(comment);
                terminal.Write(magic);
                terminal.Write(expiration);
                terminal.Write(arrow_color);
                var command = terminal.ReadCommand();
                if (command == MT2NCommand.Error)
                    throw new MT4Exception(terminal.ReadInt32());
                else
                    ticket = terminal.ReadInt32();
            }
            terminal.Orders.Add(this);
        }

        public Order(Terminal terminal, Symbol symbol, OrderType orderType, double volume, byte slippage)
        {
            if (orderType != OrderType.Buy && orderType != OrderType.Sell)
                throw new Exception("Only OrderType.Buy and OrderType.Sell are allowed with this constructor!");
            lock (terminal.ConnectionLock)
            {
                this.terminal = terminal;
                terminal.Write(N2MTCommand.OrderSend3);
                terminal.Write(symbol.Name);
                terminal.Write((byte)orderType);
                terminal.Write(volume);
                terminal.Write(slippage);
                var command = terminal.ReadCommand();
                if (command == MT2NCommand.Error)
                    throw new MT4Exception(terminal.ReadInt32());
                else
                    ticket = terminal.ReadInt32();
            }
            terminal.Orders.Add(this);
        }


        //bool OrderClose(	int ticket, double lots, double price, int slippage, color Color=CLR_NONE)
        /// <summary>
        /// Closes opened order.
        /// </summary>
        /// <param name="lots">Number of lots.</param>
        /// <param name="price">Preferred closing price.</param>
        /// <param name="slippage">Value of the maximum price slippage in points.</param>
        /// <param name="color">Color of the closing arrow on the chart.</param>
        public void Close(double lots, double price, byte slippage, Color? color = null)
        {
            lock (terminal.ConnectionLock)
            {
                terminal.Write(N2MTCommand.OrderClose);
                terminal.Write(ticket);
                terminal.Write(lots);
                terminal.Write(price);
                terminal.Write(slippage);
                terminal.Write(color);
                int result = terminal.ReadInt32();
                if (result != 0)
                    throw new MT4Exception(result);
            }
        }

        /// <summary>
        /// Closes an opened order by another opposite opened order.
        /// </summary>
        /// <param name="order">Opposite order.</param>
        /// <param name="color">Color of the closing arrow on the chart.</param>
        public void CloseBy(Order order, Color? color = null)
        {
            if (order == null)
                throw new ArgumentNullException("order");
            if (order.terminal != terminal)
                throw new ArgumentException("Order must be on same terminal.");
            lock (terminal.ConnectionLock)
            {
                terminal.Write(N2MTCommand.OrderCloseBy);
                terminal.Write(ticket);
                terminal.Write(order.ticket);
                terminal.Write(color);
                int result = terminal.ReadInt32();
                if (result != 0)
                    throw new MT4Exception(result);
            }
        }

        /// <summary>
        /// Returns close price.
        /// </summary>
        public double ClosePrice
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.OrderClosePrice);
                    terminal.Write(ticket);
                    var command = terminal.ReadCommand();
                    if (command == MT2NCommand.Error)
                        throw new MT4Exception(terminal.ReadInt32());
                    return terminal.ReadDouble();
                }
            }
        }

        public double OpenPrice
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.OrderOpenPrice);
                    terminal.Write(ticket);
                    var command = terminal.ReadCommand();
                    if (command == MT2NCommand.Error)
                        throw new MT4Exception(terminal.ReadInt32());
                    return terminal.ReadDouble();
                }
            }
        }

        internal static List<Order> GetAllOrders(Terminal terminal)
        {
            List<Order> listOfOrders = new List<Order>();
            lock (terminal.ConnectionLock)
            {
                terminal.Write(N2MTCommand.ListOrders);
                int totalOrders = terminal.ReadInt32();
                for (int i = 0; i < totalOrders; i++)
                {
                    listOfOrders.Add(new Order(terminal, terminal.ReadInt32())
                    {
                        magicNumber = terminal.ReadInt32()
                    });
                }
            }
            return listOfOrders;
        }

        public int MagicNumber
        {
            get
            {
                return magicNumber;
            }
        }

        internal void Modify(double price, double stopLoss, double takeProfit, TimeSpan? expiration = null, Color? color = null)
        {
            lock (terminal.ConnectionLock)
            {
                terminal.Write(N2MTCommand.OrderModify);
                terminal.Write(ticket);
                terminal.Write(price);
                terminal.Write(stopLoss);
                terminal.Write(takeProfit);
                terminal.Write(expiration);
                terminal.Write(color);
                int result = terminal.ReadInt32();
                if (result != 0)
                    throw new MT4Exception(result);
            }
        }

        public double Profit
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.OrderProfit);
                    terminal.Write(ticket);
                    var command = terminal.ReadCommand();
                    if (command == MT2NCommand.Error)
                        throw new MT4Exception(terminal.ReadInt32());
                    return terminal.ReadDouble();
                }
            }
        }

        public void Close()
        {
            lock (terminal.ConnectionLock)
            {
                terminal.Write(N2MTCommand.OrderClose2);
                terminal.Write(ticket);
                int result = terminal.ReadInt32();
                if (result != 0)
                    throw new MT4Exception(result);
            }
        }
    }
}
