//+------------------------------------------------------------------+
//|                                                 AutoTraderEA.mq4 |
//|                                                     David Karlas |
//|                                                                  |
//+------------------------------------------------------------------+

// First and last rule of NamedPipes communication Flush(); before and after Sending

#property copyright "David Karlas"
#property link      ""

//--- includes
#include "MT4DotNetConstants.mqh"

//--- input parameters
extern string  PipeName="MT4.AutoTrader.1";
       int     PH=-1;//PipeHandle
       
void Flush()
{
	FileFlush(PH);
	FileSeek(PH,0,SEEK_SET);   
}

void WriteString(string value)
{
	//Todo: Support longer strings
	int strLen=StringLen(value);
	if(strLen>127)
		strLen=127;
	FileWriteInteger(PH,strLen,1);
	FileWriteString(PH,value,strLen);
}

string ReadString()
{
	//Todo: Support longer strings
	int strLen=FileReadInteger(PH,1);
	Print("ReadString:"+strLen);
	return(FileReadString(PH,strLen));
}

//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
{
	while(!IsStopped())//Loop forever
	{
	if(PH>=0)//Close handle if open
	{
		FileClose(PH);
		PH=-1;
	}
	while(!IsStopped())//Try until succesfully connect to .Net server
	{
		PH=FileOpen("\\\\.\\pipe\\" + PipeName,FILE_READ|FILE_WRITE|FILE_BIN);
		if(PH>=0)
			break;
		Sleep(250);
		}
		Print("Connected");
		GetLastError();
		while(!IsStopped())
		{
		int command=FileReadInteger(PH,1);
		int error=GetLastError();
		if(error!=0)
		{
			Print("Error reading command("+command+"):"+error);
			break;
		}
		switch(command)
		{
			case N2MT_AccountBalance:
				Flush();
                FileWriteDouble(PH,AccountBalance());
				Flush();
			break;
			case N2MT_AccountCredit:
				Flush();
                FileWriteDouble(PH,AccountCredit());
				Flush();
			break;
			case N2MT_AccountCompany:
				Flush();
				WriteString(AccountCompany());
				Flush();
			break;
			case N2MT_AccountCurrency:
				Flush();
				WriteString(AccountCurrency());
				Flush();
			break;
			case N2MT_AccountEquity:
				Flush();
                FileWriteDouble(PH,AccountEquity());
				Flush();
			break;
			case N2MT_AccountFreeMargin:
				Flush();
                FileWriteDouble(PH,AccountFreeMargin());
				Flush();
			break;
			case N2MT_AccountFreeMarginCheck:
				string symbol=ReadString();
				int cmd=FileReadInteger(PH,1);
				double volume=FileReadDouble(PH);
				GetLastError();
				double result=AccountFreeMarginCheck(symbol,cmd,volume);
				int lastError=GetLastError();
				Flush();
				if(lastError==134)//Wiered but documentation say so...
				{
					FileWriteInteger(PH,MT2N_Error,1);
					FileWriteInteger(PH,lastError);
				}
				else
				{
					FileWriteInteger(PH,N2MT_AccountFreeMarginCheck,1);
					FileWriteDouble(PH,result);					
				}
				Flush();
			break;
			case N2MT_AccountFreeMarginMode:
				Flush();
				FileWriteInteger(PH,AccountFreeMarginMode(),1);
				Flush();
			break;
			case N2MT_AccountLeverage:
				Flush();
				FileWriteInteger(PH,AccountLeverage());
				Flush();
			break;
			case N2MT_AccountMargin:
				Flush();
				FileWriteDouble(PH,AccountMargin());
				Flush();
			break;
			case N2MT_AccountName:
				Flush();
				WriteString(AccountName());
				Flush();
			break;
			case N2MT_AccountNumber:
				Flush();
				FileWriteInteger(PH,AccountNumber());
				Flush();
			break;
			case N2MT_AccountProfit:
				Flush();
				FileWriteDouble(PH,AccountProfit());			
				Flush();
			break;
			case N2MT_AccountServer:
				Flush();
				WriteString(AccountServer());
				Flush();
			break;
			case N2MT_AccountStopoutLevel:
				Flush();
				FileWriteInteger(PH,AccountStopoutLevel());
				Flush();
			break;
			case N2MT_AccountStopoutMode:
				Flush();
				FileWriteInteger(PH,AccountStopoutMode(),1);
				Flush();
			break;
			case N2MT_IsConnected:
				Flush();
				FileWriteInteger(PH,IsConnected(),1);
				Flush();
			break;
			case N2MT_IsDemo:
				Flush();
				FileWriteInteger(PH,IsDemo(),1);
				Flush();
			break;
			case N2MT_IsDllsAllowed:
				Flush();
				FileWriteInteger(PH,IsDllsAllowed(),1);
				Flush();
			break;
			case N2MT_IsExpertEnabled:
				Flush();
				FileWriteInteger(PH,IsExpertEnabled(),1);
				Flush();
			break;
			case N2MT_IsLibrariesAllowed:
				Flush();
				FileWriteInteger(PH,IsLibrariesAllowed(),1);
				Flush();
			break;
			case N2MT_IsOptimization:
				Flush();
				FileWriteInteger(PH,IsOptimization(),1);
				Flush();
			break;
			case N2MT_IsStopped:
			//To increase performance we do nothing here
			//pourpuse of this 2second pulling is
			//that EA closes properlly if someone closes
			//or MT4 would crash...
			break;
			case N2MT_IsTesting:
				Flush();
				FileWriteInteger(PH,IsTesting(),1);
				Flush();
			break;
			case N2MT_IsTradeAllowed:
				Flush();
				FileWriteInteger(PH,IsTradeAllowed(),1);
				Flush();
			break;
			case N2MT_IsTradeContextBusy:
				Flush();
				FileWriteInteger(PH,IsTradeContextBusy(),1);
				Flush();
			break;
			case N2MT_IsVisualMode:
				Flush();
				FileWriteInteger(PH,IsVisualMode(),1);
				Flush();
			break;
			case N2MT_UninitializeReason:
				Flush();
				FileWriteInteger(PH,UninitializeReason(),1);
				Flush();
			break;
			case N2MT_OrderClose:
				bool result1 = OrderClose(FileReadInteger(PH),FileReadDouble(PH),FileReadDouble(PH),FileReadInteger(PH),FileReadInteger(PH));
				Flush();
				if(result1)
					FileWriteInteger(PH,0);
				else
					FileWriteInteger(PH,GetLastError());
				Flush();
			break;
			case N2MT_OrderCloseBy:
				bool result2 = OrderCloseBy(FileReadInteger(PH),FileReadInteger(PH),FileReadInteger(PH));
				Flush();
				if(result2)
					FileWriteInteger(PH,0);
				else
					FileWriteInteger(PH,GetLastError());

				Flush();
			break;
			case N2MT_OrderClosePrice:
			   bool resultOrderClosePrice = OrderSelect(FileReadInteger(PH),SELECT_BY_TICKET);
				int orderClosePriceLastError = GetLastError();
				Flush();
				if(resultOrderClosePrice)
				{
				  FileWriteInteger(PH,MT2N_Success,1);
				  FileWriteDouble(PH, OrderClosePrice());
				}
				else
				{
					FileWriteInteger(PH,MT2N_Error,1);
					FileWriteInteger(PH,orderClosePriceLastError);
				}
				Flush();
			break;
			case N2MT_OrderCloseTime:
				Flush();

				Flush();
			break;
			case N2MT_OrderComment:
				Flush();

				Flush();
			break;
			case N2MT_OrderCommission:
				Flush();

				Flush();
			break;
			case N2MT_OrderDelete:
				Flush();

				Flush();
			break;
			case N2MT_OrderExpiration:
				Flush();

				Flush();
			break;
			case N2MT_OrderLots:
				Flush();

				Flush();
			break;
			case N2MT_OrderMagicNumber:
				Flush();

				Flush();
			break;
			case N2MT_OrderModify:
				bool resultOrderModify = OrderModify(FileReadInteger(PH),FileReadDouble(PH),FileReadDouble(PH),FileReadDouble(PH),FileReadInteger(PH),FileReadInteger(PH));
				Flush();
				if(resultOrderModify)
					FileWriteInteger(PH,0);
				else
					FileWriteInteger(PH,GetLastError());
				Flush();
			break;
			case N2MT_OrderOpenPrice:
			   bool resultOrderOpenPrice = OrderSelect(FileReadInteger(PH),SELECT_BY_TICKET);
				int orderOpenPriceLastError = GetLastError();
				Flush();
				if(resultOrderOpenPrice)
				{
				  FileWriteInteger(PH,MT2N_Success,1);
				  FileWriteDouble(PH, OrderOpenPrice());
				}
				else
				{
					FileWriteInteger(PH,MT2N_Error,1);
					FileWriteInteger(PH,orderOpenPriceLastError);
				}
				Flush();
			break;
			case N2MT_OrderOpenTime:
				Flush();

				Flush();
			break;
			case N2MT_OrderPrint:
				Flush();

				Flush();
			break;
			case N2MT_OrderProfit:
				Flush();

				Flush();
			break;
			case N2MT_OrderSelect:
				Flush();

				Flush();
			break;
			case N2MT_OrderSend:
				int resultOrderSend = OrderSend(ReadString(),FileReadInteger(PH,1),FileReadDouble(PH),FileReadDouble(PH),FileReadInteger(PH,1),FileReadDouble(PH),FileReadDouble(PH));
				Flush();
				if(resultOrderSend<0)
				{
					FileWriteInteger(PH,MT2N_Error,1);
					FileWriteInteger(PH,GetLastError());
				}
				else
				{
					FileWriteInteger(PH,MT2N_Success,1);
					FileWriteInteger(PH,resultOrderSend);             
				}
				Flush();
			break;
			case N2MT_OrderSend2:
				int resultOrderSend2 = OrderSend(ReadString(),FileReadInteger(PH,1),FileReadDouble(PH),FileReadDouble(PH),FileReadInteger(PH,1),FileReadDouble(PH),FileReadDouble(PH),ReadString(),FileReadInteger(PH),FileReadInteger(PH),FileReadInteger(PH));
				Flush();
				if(resultOrderSend2<0)
				{
					FileWriteInteger(PH,MT2N_Error,1);
					FileWriteInteger(PH,GetLastError());
				}
				else
				{
					FileWriteInteger(PH,MT2N_Success,1);
					FileWriteInteger(PH,resultOrderSend2);             
				}
				Flush();			
			break;
			case N2MT_OrderSend3:
			   string orderSend3Symbol=ReadString();
			   int orderSend3Type=FileReadInteger(PH,1);
			   double orderSend3Price=0;
			   int resultOrderSend3 = 0;
			   if(orderSend3Type==OP_BUY)
			   {
			     orderSend3Price=MarketInfo(orderSend3Symbol,MODE_ASK);
				  resultOrderSend=OrderSend(orderSend3Symbol,orderSend3Type, FileReadDouble(PH),orderSend3Price,FileReadInteger(PH,1),orderSend3Price-150*MarketInfo(orderSend3Symbol,MODE_POINT),orderSend3Price+2000*MarketInfo(orderSend3Symbol,MODE_POINT));
			   }
			   else
			   {  
			      orderSend3Price=MarketInfo(orderSend3Symbol,MODE_BID);
				   resultOrderSend=OrderSend(orderSend3Symbol,orderSend3Type, FileReadDouble(PH),orderSend3Price,FileReadInteger(PH,1),orderSend3Price+150*MarketInfo(orderSend3Symbol,MODE_POINT),orderSend3Price-2000*MarketInfo(orderSend3Symbol,MODE_POINT));
				}
				Flush();
				if(resultOrderSend<0)
				{
					FileWriteInteger(PH,MT2N_Error,1);
					FileWriteInteger(PH,GetLastError());
				}
				else
				{
					FileWriteInteger(PH,MT2N_Success,1);
					FileWriteInteger(PH,resultOrderSend);             
				}
				Flush();
			break;
			case N2MT_OrdersHistoryTotal:
				Flush();

				Flush();
			break;
			case N2MT_OrderStopLoss:
				Flush();

				Flush();
			break;
			case N2MT_OrdersTotal:
				Flush();

				Flush();
			break;
			case N2MT_OrderSwap:
				Flush();

				Flush();
			break;
			case N2MT_OrderSymbol:
				Flush();

				Flush();
			break;
			case N2MT_OrderTakeProfit:
				Flush();

				Flush();
			break;
			case N2MT_OrderTicket:
				Flush();

				Flush();
			break;
			case N2MT_OrderType:
				Flush();

				Flush();
			break;
			case N2MT_Alert:
				Flush();

				Flush();
			break;
			case N2MT_Comment:
				Flush();

				Flush();
			break;
			case N2MT_MarketInfo:
            double resultMarketInfo=MarketInfo(ReadString(),FileReadInteger(PH,1));
				Flush();
				FileWriteDouble(PH,resultMarketInfo);
				Flush();
			break;
			case N2MT_MessageBox:
				Flush();

				Flush();
			break;
			case N2MT_Print:
				Flush();

				Flush();
			break;
			case N2MT_iBars:
				Flush();

				Flush();
			break;
			case N2MT_iBarShift:
				Flush();

				Flush();
			break;
			case N2MT_iClose:
				Flush();

				Flush();
			break;
			case N2MT_iHigh:
				Flush();

				Flush();
			break;
			case N2MT_iHighest:
				Flush();

				Flush();
			break;
			case N2MT_iLow:
				Flush();

				Flush();
			break;
			case N2MT_iLowest:
				Flush();

				Flush();
			break;
			case N2MT_iOpen:
				Flush();

				Flush();
			break;
			case N2MT_iTime:
				Flush();

				Flush();
			break;
			case N2MT_iVolume:
				Flush();

				Flush();
			break;
			case N2MT_TerminalCompany:
				Flush();
				WriteString(TerminalCompany());
				Flush();
			break;
			case N2MT_TerminalName:
				Flush();
				WriteString(TerminalName());
				Flush();
			break;
			case N2MT_TerminalPath:
				Flush();
				WriteString(TerminalPath());
				Flush();
			break;
			case N2MT_ListOrders:
			   Flush();
			   int totalOrders=OrdersTotal();
			   FileWriteInteger(PH,totalOrders);
			   for(int i=totalOrders-1;i>=0;i--)
			      if(OrderSelect(i,SELECT_BY_POS))
			      {
			         FileWriteInteger(PH,OrderTicket());
			         FileWriteInteger(PH,OrderMagicNumber());
			      }
			      else
			      {
			         FileWriteInteger(PH,-1);
			         FileWriteInteger(PH,-1);  
			      }
			   Flush();
			break;
         }
      }   
   }
   return(0);
}
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
  {
//----
   
//----
   return(0);
  }
//+------------------------------------------------------------------+