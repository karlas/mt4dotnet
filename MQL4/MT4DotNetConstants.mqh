//+------------------------------------------------------------------+
//|                                        MT4DotNetConstants.mq4 |
//|                                                     David Karlas |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "David Karlas"
#property link      ""

//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+



// N2MT means .Net to MetaTrader commands

#define N2MT_AccountBalance 1
#define N2MT_AccountCredit 2
#define N2MT_AccountCompany 3
#define N2MT_AccountCurrency 4
#define N2MT_AccountEquity 5
#define N2MT_AccountFreeMargin 6
#define N2MT_AccountFreeMarginCheck 7
#define N2MT_AccountFreeMarginMode 8
#define N2MT_AccountLeverage 9
#define N2MT_AccountMargin 10
#define N2MT_AccountName 11
#define N2MT_AccountNumber 12
#define N2MT_AccountProfit 13
#define N2MT_AccountServer 14
#define N2MT_AccountStopoutLevel 15
#define N2MT_AccountStopoutMode 16
#define N2MT_IsConnected 17
#define N2MT_IsDemo 18
#define N2MT_IsDllsAllowed 19
#define N2MT_IsExpertEnabled 20
#define N2MT_IsLibrariesAllowed 21
#define N2MT_IsOptimization 22
#define N2MT_IsStopped 23
#define N2MT_IsTesting 24
#define N2MT_IsTradeAllowed 25
#define N2MT_IsTradeContextBusy 26
#define N2MT_IsVisualMode 27
#define N2MT_UninitializeReason 28
#define N2MT_OrderClose 29
#define N2MT_OrderCloseBy 30
#define N2MT_OrderClosePrice 31
#define N2MT_OrderCloseTime 32
#define N2MT_OrderComment 33
#define N2MT_OrderCommission 34
#define N2MT_OrderDelete 35
#define N2MT_OrderExpiration 36
#define N2MT_OrderLots 37
#define N2MT_OrderMagicNumber 38
#define N2MT_OrderModify 39
#define N2MT_OrderOpenPrice 40
#define N2MT_OrderOpenTime 41
#define N2MT_OrderPrint 42
#define N2MT_OrderProfit 43
#define N2MT_OrderSelect 44
#define N2MT_OrderSend 45
#define N2MT_OrdersHistoryTotal 46
#define N2MT_OrderStopLoss 47
#define N2MT_OrdersTotal 48
#define N2MT_OrderSwap 49
#define N2MT_OrderSymbol 50
#define N2MT_OrderTakeProfit 51
#define N2MT_OrderTicket 52
#define N2MT_OrderType 53
#define N2MT_Alert 54
#define N2MT_Comment 55
#define N2MT_MarketInfo 56
#define N2MT_MessageBox 57
#define N2MT_Print 58
#define N2MT_iBars 59
#define N2MT_iBarShift 60
#define N2MT_iClose 61
#define N2MT_iHigh 62
#define N2MT_iHighest 63
#define N2MT_iLow 64
#define N2MT_iLowest 65
#define N2MT_iOpen 66
#define N2MT_iTime 67
#define N2MT_iVolume 68
#define N2MT_TerminalCompany 69
#define N2MT_TerminalName 70
#define N2MT_TerminalPath 71
#define N2MT_OrderSend2 72
#define N2MT_ListOrders 73
#define N2MT_OrderSend3 74


// MT2N means MetaTrader to .Net commands

#define MT2N_Success 1
#define MT2N_Error 2