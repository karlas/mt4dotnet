﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT4DotNet
{
    public class Account
    {
        Terminal terminal;
        public Account(Terminal terminal)
        {
            this.terminal = terminal;
        }

        /// <summary>
        /// Returns balance value of the current account (the amount of money on the account).
        /// </summary>
        public double Balance
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountBalance);
                    return terminal.ReadDouble();
                }
            }
        }

        /// <summary>
        /// Returns credit value of the current account.
        /// </summary>
        public double Credit
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountCredit);
                    return terminal.ReadDouble();
                }
            }
        }

        /// <summary>
        /// Returns the brokerage company name where the current account was registered.
        /// </summary>
        public string Company
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountCompany);
                    return terminal.ReadString();
                }
            }
        }

        /// <summary>
        /// Returns currency name of the current account.
        /// </summary>
        public string Currency
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountCurrency);
                    return terminal.ReadString();
                }
            }
        }

        /// <summary>
        /// Returns equity value of the current account. Equity calculation depends on trading server settings.
        /// </summary>
        public double Equity
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountEquity);
                    return terminal.ReadDouble();
                }
            }
        }

        /// <summary>
        /// Returns free margin value of the current account.
        /// </summary>
        public double FreeMargin
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountFreeMargin);
                    return terminal.ReadDouble();
                }
            }
        }

        /// <summary>
        /// Returns free margin that remains after the specified position has been opened at the current price on the current account. If the free margin is insufficient, an error 134 (ERR_NOT_ENOUGH_MONEY) will be generated.
        /// </summary>
        /// <param name="symbol">Symbol name</param>
        /// <param name="cmd">Only OrderType.Buy and OrderType.Sell are acceptable</param>
        /// <param name="volume">Volume in lots</param>
        /// <returns></returns>
        public double FreeMarginCheck(string symbol, OrderType cmd, double volume)
        {
            if (cmd != OrderType.Buy && cmd != OrderType.Sell)
                throw new ArgumentException("Only Buy and Sell OrderTypes are allowed.");
            lock (terminal.ConnectionLock)
            {
                terminal.Write(N2MTCommand.AccountFreeMarginCheck);
                terminal.Write(symbol);
                terminal.Write((byte)cmd);
                terminal.Write(volume);
                if ((MT2NCommand)terminal.ReadByte() == MT2NCommand.Success)
                    return terminal.ReadDouble();
                else
                    throw new MT4Exception(terminal.ReadInt32());
            }
        }

        public enum FreeMarginMode : byte
        {
            /// <summary>
            /// Floating profit/loss is not used for calculation
            /// </summary>
            NoProfitOrLoss = 0,
            /// <summary>
            /// Both floating profit and loss on open positions on the current account are used for free margin calculation
            /// </summary>
            BothProfitAndLoss = 1,
            /// <summary>
            /// Only profit value is used for calculation, the current loss on open positions is not considered
            /// </summary>
            OnlyProfit = 2,
            /// <summary>
            /// Only loss value is used for calculation, the current loss on open positions is not considered
            /// </summary>
            OnlyLoss = 3
        }

        /// <summary>
        /// Calculation mode of free margin allowed to open positions on the current account.
        /// </summary>
        public FreeMarginMode AccountFreeMarginMode
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountFreeMarginMode);
                    return (FreeMarginMode)terminal.ReadByte();
                }
            }
        }

        /// <summary>
        /// Returns leverage of the current account.
        /// </summary>
        public int Leverage
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountLeverage);
                    return terminal.ReadInt32();
                }
            }
        }

        /// <summary>
        /// Returns margin value of the current account.
        /// </summary>
        public double Margin
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountMargin);
                    return terminal.ReadDouble();
                }
            }
        }

        /// <summary>
        /// Returns the current account name.
        /// </summary>
        public string AccountName
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountName);
                    return terminal.ReadString();
                }
            }
        }

        /// <summary>
        /// Returns the number of the current account.
        /// </summary>
        public int AccountNumber
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountNumber);
                    return terminal.ReadInt32();
                }
            }
        }

        /// <summary>
        /// Returns profit value of the current account.
        /// </summary>
        public double Profit
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountProfit);
                    return terminal.ReadDouble();
                }
            }
        }

        /// <summary>
        /// Returns the connected server name.
        /// </summary>
        public string Server
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountServer);
                    return terminal.ReadString();
                }
            }
        }

        /// <summary>
        /// Returns the value of the Stop Out level.
        /// </summary>
        public int StopoutLevel
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountStopoutLevel);
                    return terminal.ReadInt32();
                }
            }
        }

        public enum StopOutCalculationModes : byte
        {
            /// <summary>
            /// Calculation of percentage ratio between margin and equity
            /// </summary>
            Percentage = 0,
            /// <summary>
            /// Comparison of the free margin level to the absolute value
            /// </summary>
            Absolute = 1
        }

        /// <summary>
        /// Returns the calculation mode for the Stop Out level.
        /// </summary>
        public StopOutCalculationModes StopoutMode
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.AccountStopoutMode);
                    return (StopOutCalculationModes)terminal.ReadByte();
                }
            }
        }
    }
}
