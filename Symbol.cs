﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using NDde.Client;

namespace MT4DotNet
{
    public class Symbol
    {
        public bool DdeListening { get; internal set; }
        public event Action<Symbol> BidChanged;
        public event Action<Symbol> AskChanged;

        public string Name { get; private set; }
        public string Description { get; private set; }
        private Terminal terminal;
        private Symbol(Terminal terminal, string name, string description)
        {
            Name = name;
            Description = description;
            this.terminal = terminal;
        }

        DdeClient ddeClientBid;
        DdeClient ddeClientAsk;
        public void StartDDEListening()
        {
            ddeClientAsk = new DdeClient("MT4", "ASK");
            ddeClientAsk.Disconnected += new EventHandler<DdeDisconnectedEventArgs>(ddeClient_Disconnected);
            ddeClientAsk.Advise += new EventHandler<DdeAdviseEventArgs>(ddeClient_AdviseAsk);
            ddeClientAsk.StartAdvise(Name, 1, true, 60000);
            try
            {
                ddeClientAsk.Connect();
            }
            catch
            {
                ddeClientAsk.Disconnected -= new EventHandler<DdeDisconnectedEventArgs>(ddeClient_Disconnected);
                ddeClientAsk.Advise -= new EventHandler<DdeAdviseEventArgs>(ddeClient_AdviseBid);
                ddeClientAsk.Dispose();
                ddeClientAsk = null;
                throw;
            }

            ddeClientBid = new DdeClient("MT4", "BID");
            ddeClientBid.Disconnected += new EventHandler<DdeDisconnectedEventArgs>(ddeClient_Disconnected);
            ddeClientBid.Advise += new EventHandler<DdeAdviseEventArgs>(ddeClient_AdviseBid);
            ddeClientBid.StartAdvise(Name, 1, true, 60000);
            try
            {
                ddeClientBid.Connect();
            }
            catch
            {
                ddeClientBid.Disconnected -= new EventHandler<DdeDisconnectedEventArgs>(ddeClient_Disconnected);
                ddeClientBid.Advise -= new EventHandler<DdeAdviseEventArgs>(ddeClient_AdviseAsk);
                ddeClientBid.Dispose();
                ddeClientBid = null;
                throw;
            }
        }

        public void StopDDEListening()
        {
            if (ddeClientAsk != null)
            {
                ddeClientAsk.Disconnect();
                ddeClientAsk.Dispose();
                ddeClientAsk = null;
            }

            if (ddeClientBid != null)
            {
                ddeClientBid.Disconnect();
                ddeClientBid.Dispose();
                ddeClientBid = null;
            }
        }



        void ddeClient_AdviseAsk(object sender, DdeAdviseEventArgs e)
        {
        }
        void ddeClient_AdviseBid(object sender, DdeAdviseEventArgs e)
        {
        }

        void ddeClient_Disconnected(object sender, DdeDisconnectedEventArgs e)
        {

        }


        private enum MarketInfoPullMode : byte
        {
            ///<summary>
            /// Low day price.
            ///</summary>
            Low = 1,
            ///<summary>
            /// High day price.
            ///</summary>
            High = 2,
            ///<summary>
            /// The last incoming tick time (last known server time).
            ///</summary>
            Time = 5,
            ///<summary>
            /// Last incoming bid price. For the current symbol, it is stored in the predefined variable Bid
            ///</summary>
            Bid = 9,
            ///<summary>
            /// Last incoming ask price. For the current symbol, it is stored in the predefined variable Ask
            ///</summary>
            Ask = 10,
            ///<summary>
            /// Point size in the quote currency. For the current symbol, it is stored in the predefined variable Point
            ///</summary>
            Point = 11,
            ///<summary>
            /// Count of digits after decimal point in the symbol prices. For the current symbol, it is stored in the predefined variable Digits
            ///</summary>
            Digits = 12,
            ///<summary>
            /// Spread value in points.
            ///</summary>
            Spread = 13,
            ///<summary>
            /// Stop level in points.
            ///</summary>
            StopLevel = 14,
            ///<summary>
            /// Lot size in the base currency.
            ///</summary>
            LotSize = 15,
            ///<summary>
            /// Tick value in the deposit currency.
            ///</summary>
            TickValue = 16,
            ///<summary>
            /// Tick size in points.
            ///</summary>
            TickSize = 17,
            ///<summary>
            /// Swap of the long position.
            ///</summary>
            SwapLong = 18,
            ///<summary>
            /// Swap of the short position.
            ///</summary>
            SwapShort = 19,
            ///<summary>
            /// Market starting date (usually used for futures).
            ///</summary>
            Starting = 20,
            ///<summary>
            /// Market expiration date (usually used for futures).
            ///</summary>
            Expiration = 21,
            ///<summary>
            /// Trade is allowed for the symbol.
            ///</summary>
            TradeAllowed = 22,
            ///<summary>
            /// Minimum permitted amount of a lot.
            ///</summary>
            MinLot = 23,
            ///<summary>
            /// Step for changing lots.
            ///</summary>
            LotStep = 24,
            ///<summary>
            /// Maximum permitted amount of a lot.
            ///</summary>
            MaxLot = 25,
            ///<summary>
            /// Swap calculation method. 0 - in points; 1 - in the symbol base currency; 2 - by interest; 3 - in the margin currency.
            ///</summary>
            SwapType = 26,
            ///<summary>
            /// Profit calculation mode. 0 - Forex; 1 - CFD; 2 - Futures.
            ///</summary>
            ProfitCalcMode = 27,
            ///<summary>
            /// Margin calculation mode. 0 - Forex; 1 - CFD; 2 - Futures; 3 - CFD for indices.
            ///</summary>
            MarginCalcMode = 28,
            ///<summary>
            /// Initial margin requirements for 1 lot.
            ///</summary>
            MarginInit = 29,
            ///<summary>
            /// Margin to maintain open positions calculated for 1 lot.
            ///</summary>
            MarginMaintenance = 30,
            ///<summary>
            /// Hedged margin calculated for 1 lot.
            ///</summary>
            MarginHedged = 31,
            ///<summary>
            /// Free margin required to open 1 lot for buying.
            ///</summary>
            MarginRequired = 32,
            ///<summary>
            /// Order freeze level in points. If the execution price lies within the range defined by the freeze level, the order cannot be modified, cancelled or closed.
            ///</summary>
            FreezeLevel = 33
        }


        /// <summary>
        /// Low day price.
        /// </summary>
        public double Low
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.MarketInfo);
                    terminal.Write(Name);
                    terminal.Write((byte)MarketInfoPullMode.Low);
                    return terminal.ReadDouble();
                }
            }
        }


        /// <summary>
        /// High day price.
        /// </summary>
        public double High
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.MarketInfo);
                    terminal.Write(Name);
                    terminal.Write((byte)MarketInfoPullMode.High);
                    return terminal.ReadDouble();
                }
            }
        }


        /// <summary>
        /// The last incoming tick time (last known server time).
        /// </summary>
        public double Time
        {
            get
            {
                lock (terminal.ConnectionLock)
                {
                    terminal.Write(N2MTCommand.MarketInfo);
                    terminal.Write(Name);
                    terminal.Write((byte)MarketInfoPullMode.Time);
                    return terminal.ReadDouble();
                }
            }
        }


        double bid = double.NaN;
        /// <summary>
        /// Last incoming bid price. For the current symbol, it is stored in the predefined variable Bid
        /// </summary>
        public double Bid
        {
            internal set
            {
                bid = value;
                if (BidChanged != null)
                    BidChanged(this);
            }
            get
            {
                if (DdeListening && !double.IsNaN(bid))
                {
                    return bid;
                }
                else
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.Bid);
                        return terminal.ReadDouble();
                    }
                }
            }
        }

        double ask = double.NaN;
        /// <summary>
        /// Last incoming ask price. For the current symbol, it is stored in the predefined variable Ask
        /// </summary>
        public double Ask
        {
            internal set
            {
                ask = value;
                if (AskChanged != null)
                    AskChanged(this);
            }
            get
            {
                if (DdeListening && !double.IsNaN(ask))
                {
                    return ask;
                }
                else
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.Ask);
                        return terminal.ReadDouble();
                    }
                }
            }
        }


        private double point = double.NaN;

        /// <summary>
        /// BUFFERED: Point size in the quote currency. For the current symbol, it is stored in the predefined variable Point
        /// </summary>
        public double Point
        {
            get
            {
                if (double.IsNaN(point))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.Point);
                        point = terminal.ReadDouble();
                    }
                }
                return point;
            }
        }


        private double digits = double.NaN;

        /// <summary>
        /// BUFFERED: Count of digits after decimal point in the symbol prices. For the current symbol, it is stored in the predefined variable Digits
        /// </summary>
        public double Digits
        {
            get
            {
                if (double.IsNaN(digits))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.Digits);
                        digits = terminal.ReadDouble();
                    }
                }
                return digits;
            }
        }


        private double spread = double.NaN;

        /// <summary>
        /// BUFFERED: Spread value in points.
        /// </summary>
        public double Spread
        {
            get
            {
                if (double.IsNaN(spread))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.Spread);
                        spread = terminal.ReadDouble();
                    }
                }
                return spread;
            }
        }


        private double stopLevel = double.NaN;

        /// <summary>
        /// BUFFERED: Stop level in points.
        /// </summary>
        public double StopLevel
        {
            get
            {
                if (double.IsNaN(stopLevel))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.StopLevel);
                        stopLevel = terminal.ReadDouble();
                    }
                }
                return stopLevel;
            }
        }


        private double lotSize = double.NaN;

        /// <summary>
        /// BUFFERED: Lot size in the base currency.
        /// </summary>
        public double LotSize
        {
            get
            {
                if (double.IsNaN(lotSize))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.LotSize);
                        lotSize = terminal.ReadDouble();
                    }
                }
                return lotSize;
            }
        }


        private double tickValue = double.NaN;

        /// <summary>
        /// BUFFERED: Tick value in the deposit currency.
        /// </summary>
        public double TickValue
        {
            get
            {
                if (double.IsNaN(tickValue))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.TickValue);
                        tickValue = terminal.ReadDouble();
                    }
                }
                return tickValue;
            }
        }


        private double tickSize = double.NaN;

        /// <summary>
        /// BUFFERED: Tick size in points.
        /// </summary>
        public double TickSize
        {
            get
            {
                if (double.IsNaN(tickSize))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.TickSize);
                        tickSize = terminal.ReadDouble();
                    }
                }
                return tickSize;
            }
        }


        private double swapLong = double.NaN;

        /// <summary>
        /// BUFFERED: Swap of the long position.
        /// </summary>
        public double SwapLong
        {
            get
            {
                if (double.IsNaN(swapLong))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.SwapLong);
                        swapLong = terminal.ReadDouble();
                    }
                }
                return swapLong;
            }
        }


        private double swapShort = double.NaN;

        /// <summary>
        /// BUFFERED: Swap of the short position.
        /// </summary>
        public double SwapShort
        {
            get
            {
                if (double.IsNaN(swapShort))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.SwapShort);
                        swapShort = terminal.ReadDouble();
                    }
                }
                return swapShort;
            }
        }


        private double starting = double.NaN;

        /// <summary>
        /// BUFFERED: Market starting date (usually used for futures).
        /// </summary>
        public double Starting
        {
            get
            {
                if (double.IsNaN(starting))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.Starting);
                        starting = terminal.ReadDouble();
                    }
                }
                return starting;
            }
        }


        private double expiration = double.NaN;

        /// <summary>
        /// BUFFERED: Market expiration date (usually used for futures).
        /// </summary>
        public double Expiration
        {
            get
            {
                if (double.IsNaN(expiration))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.Expiration);
                        expiration = terminal.ReadDouble();
                    }
                }
                return expiration;
            }
        }


        private double tradeAllowed = double.NaN;

        /// <summary>
        /// BUFFERED: Trade is allowed for the symbol.
        /// </summary>
        public double TradeAllowed
        {
            get
            {
                if (double.IsNaN(tradeAllowed))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.TradeAllowed);
                        tradeAllowed = terminal.ReadDouble();
                    }
                }
                return tradeAllowed;
            }
        }


        private double minLot = double.NaN;

        /// <summary>
        /// BUFFERED: Minimum permitted amount of a lot.
        /// </summary>
        public double MinLot
        {
            get
            {
                if (double.IsNaN(minLot))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.MinLot);
                        minLot = terminal.ReadDouble();
                    }
                }
                return minLot;
            }
        }


        private double lotStep = double.NaN;

        /// <summary>
        /// BUFFERED: Step for changing lots.
        /// </summary>
        public double LotStep
        {
            get
            {
                if (double.IsNaN(lotStep))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.LotStep);
                        lotStep = terminal.ReadDouble();
                    }
                }
                return lotStep;
            }
        }


        private double maxLot = double.NaN;

        /// <summary>
        /// BUFFERED: Maximum permitted amount of a lot.
        /// </summary>
        public double MaxLot
        {
            get
            {
                if (double.IsNaN(maxLot))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.MaxLot);
                        maxLot = terminal.ReadDouble();
                    }
                }
                return maxLot;
            }
        }


        private double swapType = double.NaN;

        /// <summary>
        /// BUFFERED: Swap calculation method. 0 - in points; 1 - in the symbol base currency; 2 - by interest; 3 - in the margin currency.
        /// </summary>
        public double SwapType
        {
            get
            {
                if (double.IsNaN(swapType))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.SwapType);
                        swapType = terminal.ReadDouble();
                    }
                }
                return swapType;
            }
        }


        private double profitCalcMode = double.NaN;

        /// <summary>
        /// BUFFERED: Profit calculation mode. 0 - Forex; 1 - CFD; 2 - Futures.
        /// </summary>
        public double ProfitCalcMode
        {
            get
            {
                if (double.IsNaN(profitCalcMode))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.ProfitCalcMode);
                        profitCalcMode = terminal.ReadDouble();
                    }
                }
                return profitCalcMode;
            }
        }


        private double marginCalcMode = double.NaN;

        /// <summary>
        /// BUFFERED: Margin calculation mode. 0 - Forex; 1 - CFD; 2 - Futures; 3 - CFD for indices.
        /// </summary>
        public double MarginCalcMode
        {
            get
            {
                if (double.IsNaN(marginCalcMode))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.MarginCalcMode);
                        marginCalcMode = terminal.ReadDouble();
                    }
                }
                return marginCalcMode;
            }
        }


        private double marginInit = double.NaN;

        /// <summary>
        /// BUFFERED: Initial margin requirements for 1 lot.
        /// </summary>
        public double MarginInit
        {
            get
            {
                if (double.IsNaN(marginInit))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.MarginInit);
                        marginInit = terminal.ReadDouble();
                    }
                }
                return marginInit;
            }
        }


        private double marginMaintenance = double.NaN;

        /// <summary>
        /// BUFFERED: Margin to maintain open positions calculated for 1 lot.
        /// </summary>
        public double MarginMaintenance
        {
            get
            {
                if (double.IsNaN(marginMaintenance))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.MarginMaintenance);
                        marginMaintenance = terminal.ReadDouble();
                    }
                }
                return marginMaintenance;
            }
        }


        private double marginHedged = double.NaN;

        /// <summary>
        /// BUFFERED: Hedged margin calculated for 1 lot.
        /// </summary>
        public double MarginHedged
        {
            get
            {
                if (double.IsNaN(marginHedged))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.MarginHedged);
                        marginHedged = terminal.ReadDouble();
                    }
                }
                return marginHedged;
            }
        }


        private double marginRequired = double.NaN;

        /// <summary>
        /// BUFFERED: Free margin required to open 1 lot for buying.
        /// </summary>
        public double MarginRequired
        {
            get
            {
                if (double.IsNaN(marginRequired))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.MarginRequired);
                        marginRequired = terminal.ReadDouble();
                    }
                }
                return marginRequired;
            }
        }


        private double freezeLevel = double.NaN;

        /// <summary>
        /// BUFFERED: Order freeze level in points. If the execution price lies within the range defined by the freeze level, the order cannot be modified, cancelled or closed.
        /// </summary>
        public double FreezeLevel
        {
            get
            {
                if (double.IsNaN(freezeLevel))
                {
                    lock (terminal.ConnectionLock)
                    {
                        terminal.Write(N2MTCommand.MarketInfo);
                        terminal.Write(Name);
                        terminal.Write((byte)MarketInfoPullMode.FreezeLevel);
                        freezeLevel = terminal.ReadDouble();
                    }
                }
                return freezeLevel;
            }
        }


        public override string ToString()
        {
            return Name + " - " + Description;
        }

        internal static Symbol[] GetSymbolList(Terminal terminal)
        {
            using (FileStream FS = new FileStream(terminal.StartupPath + "\\history\\" + terminal.Account.Server + "\\symbols.raw", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                var symbols = new Symbol[FS.Length / 1936];

                for (int i = 0; i < symbols.Length; i++)
                {
                    byte[] nameBytes = new byte[12];
                    if (FS.Read(nameBytes, 0, 12) != 12)
                        throw new InvalidOperationException("Failed to read data from symbol file.");
                    byte[] descriptionBytes = new byte[64];
                    if (FS.Read(descriptionBytes, 0, 64) != 64)
                        throw new InvalidOperationException("Failed to read data from symbol file.");
                    var nameLength = nameBytes.TakeWhile(b => b != 0).Count();
                    var descriptionLength = descriptionBytes.TakeWhile(b => b != 0).Count();
                    symbols[i] = new Symbol(terminal, Encoding.ASCII.GetString(nameBytes, 0, nameLength), Encoding.ASCII.GetString(descriptionBytes, 0, descriptionLength));
                    FS.Seek(1860, SeekOrigin.Current);
                }
                return symbols;
            }
        }

    }
}
